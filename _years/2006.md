---
organiser: Marc Holmes
venue: Earl's Court
---

Chief Steward
: Robin Lacy

Head of Technical
: Kim Kellaway

CBOB winner
: Crouch Vale, Brewers Gold

Logo
: Ring with CAMRA Logo [Picture]({% file logo.jpg %})


Links
: [Programme]({% file programme.jpg %})
[Floor plan]({% file floorplan.jpg %})

### Opening hours
Tuesday	
: Trade 12pm - 5pm, Public 5pm - 10:30pm

Wednesday
: 12 noon - 10:30pm

Thursday
: 12 noon - 10:30pm

Friday
: 12 noon - 10:30pm

Saturday
: 11am - 7pm

### Sponsors and supporters

Charity
: Unknown

Staff T-shirt
: Hobgoblin, Lager Boys
[Front]({% file shirt_front.jpg %})
[Back]({% file shirt_back.jpg %})

### Bars

**Warwick Stillage**

W1
: Daigy Thomas (Wolverhampton & Dudley, Hook Norton, Theakstons, McMullen)

W2
: Steve Prescott (Badger, Greene King, Sharps, Thwaites, Caledonian)

W3
: John Lewis (Cider) 

W4
: John Cornish (Youngs, Shepherd Neame, Hogs Back, Fullers)

W5
: Kiz Mackisack (Charles Wells, Archers, St Austell, Cains)

**Brompton Stillage**

B1
: Andy Mitchell 

B2
: Ron Ridout 

B3
: Colin Valentine 

B4
: Dave Oram 

B5
: Ben Parr-Ferris & Bobby Scully 

B6
: Buster Grant 

B7
: John Chapman (Real Ale in a Bottle)

**Philbeach Stillage**

P1
: Chris Overman 

P2
: Nick Banyard & Brian Francis 

P3
: Keith Jenkins 

P4
: Bagsy Durham 

P5
: John Swingler 

P6
: Doug Smith 

P7
: Roger Mayhew 

P8
: Dave Sanders

### Entertainment

Tuesday Eve
: Chaminade

Wednesday lunch
: Ebony Steel Band

Wednesday evening
: Feast of Fiddles

Thursday lunch
: Phil Bates (ex ELO)

Thursday evening
: M3 classic Whitesnake

Friday lunch
: Acoustic Strawbs

Friday evening
: Bootleg Abba

Saturday afternoon
: Denham Hendon Brass Band

### Behind the scenes

Wenches' Night Theme	
: Bunny Girls

Night Stewards Prank
: The Village Pond

Christian Muteau Award	
: John Cornish
