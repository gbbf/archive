---
organiser: Marc Holmes
venue: Earl's Court
banner: banner.jpg
---

Chief Steward
: Duncan Ward

Head of Technical
: Chris Overman

CBOB winner
: Castle Rock, Harvest Pale

Main Campaign
: Campaign of Two Halves

Logo
: Britannia and Lion on Shield [Picture]({% file logo.jpg %})

Links
: [Programme]({% file programme.jpg %})
[Floor plan]({% file floorplan.jpg %})

### Opening hours
Tuesday	
: Trade 12pm - 5pm, Public 5pm - 10:30pm

Wednesday
: 12 noon - 10:30pm

Thursday
: 12 noon - 10:30pm

Friday
: 12 noon - 10:30pm

Saturday
: 11am - 7pm

### Sponsors and supporters

Charity
: Royal British Legion

Staff T-shirt
: St Austell, Tribute & Kitchener
[Front]({% file shirt_front.jpg %})
[Back]({% file shirt_back.jpg %})


Glases
: Marstons 
[Image 1]({% file thirdglass_logo.jpg %})
[Image 2]({% file thirdglass_sponsor1.jpg %})
[Image 3]({% file thirdglass_sponsor2.jpg %})

### Bars

This year the bars were named after heroes.

**Warwick Stillage**

W1
: Jim Strang (BSF: German & Czech) 

W2
: Dave Sanders (BSF: USA & Rest of the World) 

W3
: Roger Mayhew (BSF: Belgian & Dutch) 

W4
: John Swingler 

W5
: John Lewis (Cider) 

W6
: Bagsy Durham


**Brompton Stillage**

B1
: Kiz Mackisack (Brewery Bar: Brains, SIBA, Thornbridge, Thwaites)

B2
: Nick Banyard

B3
: Aileen MacKinnon  (Brewery Bar: Theakstons, Wells & Youngs, Liberation)

B4
: Dave Oram

B5
: Ben Parr-Ferris

B6
: Bobby Scully

B7
: Ian Hill & Gareth Bowman

B8
: Hannah Pettifer (Real Ale in a Bottle)

**Philbeach Stillage**

P1
: Mike Labbe

P2 & P3
: Steve Prescott (Brewery Bar: Shepherd Neame, Marstons, Greene King, Fullers)

P4
: Colin Valentine

P5
: James Linehan

P6
: Keith Jenkins

P7
: Graham Collier

P8
: Buster Grant

### Entertainment

Tuesday evening
: 	Chaminade

Wednesday lunch
: 	Band of Two

Wednesday evening
:	Hank Wangford and the Lost Cowboys

Thursday lunch
:	Fret and Fiddle

Thursday evening
:	The Blockheads

Friday lunch
:	Gary Fletcher Band

Friday evening
:	The Hamsters

Saturday afternoon	
: Denham Hendon Brass Band

### Behind the scenes

Wenches' Night Theme	
: Show Girls

Night Stewards Pranks	
: My Little Ponies on Stage

Christian Muteau Award	
: Mike Brady
