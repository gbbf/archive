---
organiser: John Norman
venue: Docklands Arena, London
---

Chief Steward
: Adrian Zawierka

CBOB winner
: Mauldon, Black Adder

Logo
: Tower Bridge

Links
: [Logo]({% file logo.jpg %})


### Entertainment

Tuesday Eve
: Chaminade String Quartet
