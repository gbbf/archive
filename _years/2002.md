---
organiser: Marc Holmes
venue: Olympia
---

Chief Steward
: Duncan Ward

CBOB winner
: Caledonian, Deuchars IPA

Main campaign
: Ninkasi

Logo
: Union Flag around a pint [Picture]({% file logo.jpg %})

Halls
: Grand, National, West

Links
: [Programme]({% file programme.jpg %})
[Floor plan]({% file floorplan.jpg %})
[Sweat shirt]({% file sweatshirt.jpg %})
[Beer list]({% file beerlist.pdf %})
[Tasting notes]({% file tastingnotes.pdf %})

### Opening hours
Tuesday	
: Trade 12pm - 5pm, Public 5pm - 10:30pm

Wednesday
: 12 noon - 10:30pm

Thursday
: 12 noon - 10:30pm

Friday
: 12 noon - 10:30pm

Saturday
: 11am - 7pm

### Sponsors and supporters

Charity
: Macmillan Cancer Relief

### Bars

10 + Brewery Bars + RAIB + Cider + BSF

**Brewery Bars**
Kiz
: Shepherd Neame, Adnams, Batemans, Everards

Ron
: Gales, Marstons, Fullers, Youngs, 

Linda
: St Austell, Greene King, Arkells, Hardy & Hansons 

Steve & Nick
: Scottish Courage, Cains, Badger, Charles Wells

Paul
: Beer Seller

### Entertainment

Tuesday Eve
: Chaminade

Wednesday evening
: Humphrey Lyttelton

Thursday lunch
: Paul Jones & Dave Kelly from the Blues Band

Thursday evening
: Kit Packham and One Jump Ahead

Friday lunch
: ZigaZag! Cajun Band

Friday evening
: John Otway

Saturday afternoon
: Fulham Brass Band

### Behind the scenes

Wenches' Night Theme	
: Schoolgirls

Christian Muteau Award	
: Kevin Reeve
