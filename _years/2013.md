---
organiser: Ian Hill
venue: Olympia
---

Chief Steward
: Duncan Ward

Head of Technical
: Chris Overman

Logo
: Britannia and Lion [Picture]({% file logo.jpg %})

Theme
: Parachuting beers

### Opening hours
Tuesday
: Trade 12pm - 5pm, Public 5pm - 10:30pm

Wednesday
: 12 noon - 10.30pm

Thursday
: 12 noon - 10.30pm

Friday
: 12 noon - 10.30pm

Saturday
: 11am - 7pm

### Sponsors and supporters

Glasses
: Nicholsons

### Bars

*Grand Hall*
B1
: Colin Dingwall (BSF: Belgian & Dutch)

*Grand Stillage*
B2
: Keith Jenkins

B3
: Steve Prescott (Brewery Bar)

*Staircase Stillage*
B4
: Graham Collier

B5
: Bobbie Scully

B6a
: Hanna Pettifer (RAIB)

B6b
: Kerr Kennedy (BSF: Rest of the World)

B7
: Chris Rouse (Cider)

B8
: Dave Oram 

B9
: Ben Parr-Ferris

*Grand Stillage*
B10
: Jon Gobbett

B11
: Kiz Mackisack (Brewery Bar)

B12
: Adrian Saunders (Brewery Bar)

*National Hall*
B13
: Bagsy Durham

B14
: John Swingler

B15
: James Strang (BSF: German and Czech)

B16
: Nick Banyard

B17
: Dave Sanders (BSF: USA Cask)

B18
: Michael Labbe

B19
: Buster Grant 

