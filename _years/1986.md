---
organiser: John Cryne
venue: Metropole, Brighton
---

Chief Steward
: Ted Eller

CBOB winner
: Bateman, XXXB

Logo
: Dolphin and Brighton Pavilion

Links
: [Logo]({% file logo.jpg %})
[Logo 2]({% file logo2.jpg %})
[Article 1]({% file article1.jpg %})
[Article 2]({% file article2.jpg %})
[Photo 1]({% file photo1.jpg %})
[Photo 2]({% file photo2.jpg %})
[Photo 3]({% file photo3.jpg %})
