---
organiser: Pat O'Neill
venue: Queen's Hall, Leeds
---

CBOB winner
: Fuller's, ESB

Logo
: Mills and Pit Head

Links
: [Programme]({% file programme.jpg %})
[Floor plan]({% file floorplan.jpg %})
[Logo]({% file logo.jpg %})
[Glass]({% file glass.jpg %})
[Bar list]({% file barindex.jpg %})

### Entertainment

Wednesday lunch
: Nick Strutt & Jeremy Wolstenholme, Bob White

Wednesday evening
: The Royton Drift Mine Band, T' Bag O'Shoddy

Thursday lunch
: The Medieval Players

Thursday evening
: Ed O'Donnell Jazz Band, Hot Punch

Friday lunch
: Bill Brookman, Stutt & Wolstenholme

Friday evening
: Bill Brookman & Fanny Jones, Yorkshire Bus Company Band

Saturday lunch
: Bosco Steel Band, DCB Traditional Jazz Band

Saturday evening
: T'Bag O'Shoddy, Stanley and Newmarket Colliery Band
