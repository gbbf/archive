---
organiser: Ted Eller
venue: Queen's Hall, Leeds
---

Chief Steward
: Alan Ramsbottom

CBOB winner
: Fuller's, Chiswick Bitter

Logo
: Owl with sandwich board holding a pint

Links
: [Programme]({% file programme.jpg %})


