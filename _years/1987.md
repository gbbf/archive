---
organiser: John Cryne
venue: Metropole, Brighton
---

Chief Steward
: Ted Eller

CBOB winner
: Pitfield, Dark Star

Main campaign
: 10th Year of GBBF

Logo
: Seagull

Links
: [Logo]({% file logo.jpg %})
