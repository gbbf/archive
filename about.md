---
layout: page
title: About
permalink: /about/
---

The Great British Beer Festival is an annual celebration of real 
ale, cider and perry organised by
[The Campaign for Real Ale](http://www.camra.org.uk). For
information about the next festival, please visit
[www.gbbf.org.uk](http://www.gbbf.org.uk).

This archive of interesting GBBF information was started
by Linda Hutton and friends in 2010. It is mainly of interest
to current and former GBBF volunteers, but will also give
visitors a little glimpse behind the scenes!

If you spot any mistakes or missing information please e-mail
<ian.hill@camra.org.uk>. Alternatively, you can contribute to
the project directly at 
[https://gitlab.com/gbbf/archive](https://gitlab.com/gbbf/archive).
