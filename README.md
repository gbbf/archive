# Great British Beer Festival Archive

## Introduction

This repository is a Jekyll site containing interesting information
about the Great British Beer Festival.

## Contributing information

If you spot any mistakes or missing info please email 
<ian.hill@camra.org.uk>. 

Alternatively, you can contribute new or updated pages by
editing one of the files in [_years](/_years).

## Building

Build by simply running Jekyll in your preferred way.

I don't use Ruby for anything else so I find it convenient to use
the docker version:

```
export JEKYLL_VERSION=3.8     
docker run --rm \
  --volume="$PWD:/srv/jekyll" \
  -it jekyll/jekyll:$JEKYLL_VERSION \
  jekyll build
```

